// all variables

variable "instance_type" {
  default = "t2.micro"
}

variable "vpc_cidr_block" {
  default = "11.0.0.0/16"
}